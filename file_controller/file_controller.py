import os


def read_file(filename):
    f = open(filename, "r")
    content = f.read()
    f.close()
    return content


def append_write_file(filename, content):
    f = open(filename, "a")
    f.write(content)
    f.close()


def get_project_path():
    current_filename = os.path.splitext(os.path.basename(__file__))[0]
    current_dir = os.path.dirname(os.path.abspath(__file__))
    current_dir = current_dir.replace(os.sep, '/')
    return current_dir.replace(current_filename, '')[:-1]
