import os

from vlc_lua_controller.vlc_lua_controller import VlcLuaController
from subtitle_controller.subtitle_controller import SubtitleController
from console_ui.console_ui import ConsoleUI
from dotenv import load_dotenv

load_dotenv()


def main():
    # Initialize dependencies
    vlc = VlcLuaController(
        os.getenv('VLC_LUA_CONTROLLER_BASE_URL'),
        os.getenv('VLC_LUA_CONTROLLER_USERNAME'),
        os.getenv('VLC_LUA_CONTROLLER_PASSWORD'),
    )
    sub_controller = SubtitleController()

    # Start Script
    mode = os.getenv('MODE')
    if mode == "1":
        ui = ConsoleUI(vlc, sub_controller)
        ui.start()


if __name__ == '__main__':
    main()
