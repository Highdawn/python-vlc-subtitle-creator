import requests
import re


class VlcLuaController:
    base_url = "http://localhost:8080/requests/status.xml"
    auth = ('', '')

    def __init__(self, base_url, username, password):
        self.base_url = base_url
        self.auth = (username, password)

    def open(self, file):
        command = self.base_url + "?command=in_enqueue&input=" + str(file)
        requests.get(command, auth=self.auth)

    def play(self):
        command = self.base_url + "?command=pl_play"
        requests.get(command, auth=self.auth)

    def pause(self):
        command = self.base_url + "?command=pl_pause"
        requests.get(command, auth=self.auth)

    def stop(self):
        command = self.base_url + "?command=pl_stop"
        requests.get(command, auth=self.auth)

    def jump_to(self, time):
        command = self.base_url + "?command=seek&val=" + str(time)
        requests.get(command, auth=self.auth)

    def get_time(self):
        s = requests.Session()
        s.auth = self.auth
        r = s.get(self.base_url, verify=False)
        text = r.text
        return int(re.search('<time>(.+?)</time>', text).group(1))

    def get_filename(self):
        s = requests.Session()
        s.auth = self.auth
        r = s.get(self.base_url, verify=False)
        text = r.text
        return re.search("<info name='filename'>(.+?)</info>", text).group(1).split(".")[0]

