import os


class ConsoleUI:
    vlc = False
    sub_controller = False
    sub_default_duration = os.getenv('SUBTITLE_DEFAULT_DURATION')
    sub_default_countdown = os.getenv('SUBTITLE_DEFAULT_COUNTDOWN')

    def __init__(self, vlc, sub_controller):
        self.vlc = vlc
        self.sub_controller = sub_controller

    def start(self):
        option = "PLACEHOLDER"
        while option != 0:
            print("Select your option?")
            print("1 - Create a subtitle")
            print("2 - List current subtitles created")
            print("3 - Create file")
            print("4 - Create countdown subtitle")
            print("0 - Exit")
            option = input("")

            if option == "0":
                exit()

            if option == "1":
                current_time = self.vlc.get_time()
                content = input("Write the content of the subtitle: ")
                self.sub_controller.create_subtitle(content, current_time, 5)
                print("")
                continue

            if option == "2":
                print(self.sub_controller.get_subtitles())
                print("")
                continue

            if option == "3":
                filename = self.vlc.get_filename()
                self.sub_controller.create_file(filename)
                print("")
                continue

            if option == "4":
                current_time = self.vlc.get_time()
                content = input("Write the content of the subtitle: ")
                self.sub_controller.create_countdown_subtitles(
                    current_time,
                    content,
                    self.sub_default_duration,
                    self.sub_default_countdown
                )
                print("")
                continue

            print("Option not recognized")
