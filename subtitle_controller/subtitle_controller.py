from file_controller import file_controller as fc
import time


class SubtitleController:
    subtitles = {}
    timestamp_format = '%H:%M:%S,000'
    output_dir = fc.get_project_path() + "/output/"
    file_extension = ".ass"

    def get_subtitles(self):
        return self.subtitles

    def clear_subtitles(self):
        self.subtitles = {}

    def create_file(self, filename):
        self.subtitles = sorted(self.subtitles.items())
        sub_counter = 1
        for sub_time_start, subtitle in self.subtitles:
            index = str(sub_counter)
            ts_start = time.strftime(self.timestamp_format, self.seconds_to_timestamp(sub_time_start))
            ts_end = time.strftime(self.timestamp_format,
                                   self.seconds_to_timestamp(sub_time_start + subtitle.get("duration")))
            content = subtitle.get("content")

            new_sub = self.replace_model_content(index, ts_start, ts_end, content)
            sub_counter = sub_counter + 1
            file_path = self.output_dir + filename + self.file_extension
            fc.append_write_file(file_path, new_sub)
            fc.append_write_file(file_path, "\n")

    def create_subtitle(self, content, current_time, duration):
        self.subtitles[current_time] = {
            "duration": duration,
            "content": content
        }

    def create_countdown_subtitles(self, current_time, content, first_duration, countdown_time):
        self.create_subtitle(content, current_time, first_duration)
        counter = 1
        while counter <= countdown_time:
            self.create_subtitle(content + " in " + str(counter), current_time - counter, 1)
            counter = counter + 1
        pass

    @staticmethod
    def seconds_to_timestamp(seconds):
        print(seconds)
        return time.gmtime(seconds)

    @staticmethod
    def replace_model_content(index, ts_start, ts_end, content):
        new_subtitle = fc.read_file("subtitle_template.txt")
        new_subtitle = new_subtitle.replace("[[INDEX]]", index)
        new_subtitle = new_subtitle.replace("[[TIMESTAMP_START]]", ts_start)
        new_subtitle = new_subtitle.replace("[[TIMESTAMP_END]]", ts_end)
        new_subtitle = new_subtitle.replace("[[CONTENT]]", content)
        return new_subtitle
